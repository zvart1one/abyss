// Copyright Epic Games, Inc. All Rights Reserved.

#include "AbyssGameMode.h"
#include "AbyssPlayerController.h"
#include "AbyssCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAbyssGameMode::AAbyssGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AAbyssPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}