// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Abyss/AbyssGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbyssGameMode() {}
// Cross Module References
	ABYSS_API UClass* Z_Construct_UClass_AAbyssGameMode_NoRegister();
	ABYSS_API UClass* Z_Construct_UClass_AAbyssGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Abyss();
// End Cross Module References
	void AAbyssGameMode::StaticRegisterNativesAAbyssGameMode()
	{
	}
	UClass* Z_Construct_UClass_AAbyssGameMode_NoRegister()
	{
		return AAbyssGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AAbyssGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAbyssGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Abyss,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAbyssGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "AbyssGameMode.h" },
		{ "ModuleRelativePath", "AbyssGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAbyssGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAbyssGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAbyssGameMode_Statics::ClassParams = {
		&AAbyssGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AAbyssGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAbyssGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAbyssGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAbyssGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAbyssGameMode, 2002540083);
	template<> ABYSS_API UClass* StaticClass<AAbyssGameMode>()
	{
		return AAbyssGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAbyssGameMode(Z_Construct_UClass_AAbyssGameMode, &AAbyssGameMode::StaticClass, TEXT("/Script/Abyss"), TEXT("AAbyssGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAbyssGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
