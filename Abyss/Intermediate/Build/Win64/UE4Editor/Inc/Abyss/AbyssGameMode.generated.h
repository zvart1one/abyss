// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ABYSS_AbyssGameMode_generated_h
#error "AbyssGameMode.generated.h already included, missing '#pragma once' in AbyssGameMode.h"
#endif
#define ABYSS_AbyssGameMode_generated_h

#define Abyss_Source_Abyss_AbyssGameMode_h_12_SPARSE_DATA
#define Abyss_Source_Abyss_AbyssGameMode_h_12_RPC_WRAPPERS
#define Abyss_Source_Abyss_AbyssGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Abyss_Source_Abyss_AbyssGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAbyssGameMode(); \
	friend struct Z_Construct_UClass_AAbyssGameMode_Statics; \
public: \
	DECLARE_CLASS(AAbyssGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Abyss"), ABYSS_API) \
	DECLARE_SERIALIZER(AAbyssGameMode)


#define Abyss_Source_Abyss_AbyssGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAbyssGameMode(); \
	friend struct Z_Construct_UClass_AAbyssGameMode_Statics; \
public: \
	DECLARE_CLASS(AAbyssGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Abyss"), ABYSS_API) \
	DECLARE_SERIALIZER(AAbyssGameMode)


#define Abyss_Source_Abyss_AbyssGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ABYSS_API AAbyssGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAbyssGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ABYSS_API, AAbyssGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAbyssGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ABYSS_API AAbyssGameMode(AAbyssGameMode&&); \
	ABYSS_API AAbyssGameMode(const AAbyssGameMode&); \
public:


#define Abyss_Source_Abyss_AbyssGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ABYSS_API AAbyssGameMode(AAbyssGameMode&&); \
	ABYSS_API AAbyssGameMode(const AAbyssGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ABYSS_API, AAbyssGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAbyssGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAbyssGameMode)


#define Abyss_Source_Abyss_AbyssGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Abyss_Source_Abyss_AbyssGameMode_h_9_PROLOG
#define Abyss_Source_Abyss_AbyssGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Abyss_Source_Abyss_AbyssGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Abyss_Source_Abyss_AbyssGameMode_h_12_SPARSE_DATA \
	Abyss_Source_Abyss_AbyssGameMode_h_12_RPC_WRAPPERS \
	Abyss_Source_Abyss_AbyssGameMode_h_12_INCLASS \
	Abyss_Source_Abyss_AbyssGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Abyss_Source_Abyss_AbyssGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Abyss_Source_Abyss_AbyssGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Abyss_Source_Abyss_AbyssGameMode_h_12_SPARSE_DATA \
	Abyss_Source_Abyss_AbyssGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Abyss_Source_Abyss_AbyssGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Abyss_Source_Abyss_AbyssGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ABYSS_API UClass* StaticClass<class AAbyssGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Abyss_Source_Abyss_AbyssGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
