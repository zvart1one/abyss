// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ABYSS_AbyssPlayerController_generated_h
#error "AbyssPlayerController.generated.h already included, missing '#pragma once' in AbyssPlayerController.h"
#endif
#define ABYSS_AbyssPlayerController_generated_h

#define Abyss_Source_Abyss_AbyssPlayerController_h_12_SPARSE_DATA
#define Abyss_Source_Abyss_AbyssPlayerController_h_12_RPC_WRAPPERS
#define Abyss_Source_Abyss_AbyssPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Abyss_Source_Abyss_AbyssPlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAbyssPlayerController(); \
	friend struct Z_Construct_UClass_AAbyssPlayerController_Statics; \
public: \
	DECLARE_CLASS(AAbyssPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Abyss"), NO_API) \
	DECLARE_SERIALIZER(AAbyssPlayerController)


#define Abyss_Source_Abyss_AbyssPlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAbyssPlayerController(); \
	friend struct Z_Construct_UClass_AAbyssPlayerController_Statics; \
public: \
	DECLARE_CLASS(AAbyssPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Abyss"), NO_API) \
	DECLARE_SERIALIZER(AAbyssPlayerController)


#define Abyss_Source_Abyss_AbyssPlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAbyssPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAbyssPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAbyssPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAbyssPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAbyssPlayerController(AAbyssPlayerController&&); \
	NO_API AAbyssPlayerController(const AAbyssPlayerController&); \
public:


#define Abyss_Source_Abyss_AbyssPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAbyssPlayerController(AAbyssPlayerController&&); \
	NO_API AAbyssPlayerController(const AAbyssPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAbyssPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAbyssPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAbyssPlayerController)


#define Abyss_Source_Abyss_AbyssPlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define Abyss_Source_Abyss_AbyssPlayerController_h_9_PROLOG
#define Abyss_Source_Abyss_AbyssPlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_SPARSE_DATA \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_RPC_WRAPPERS \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_INCLASS \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Abyss_Source_Abyss_AbyssPlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_SPARSE_DATA \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_INCLASS_NO_PURE_DECLS \
	Abyss_Source_Abyss_AbyssPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ABYSS_API UClass* StaticClass<class AAbyssPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Abyss_Source_Abyss_AbyssPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
