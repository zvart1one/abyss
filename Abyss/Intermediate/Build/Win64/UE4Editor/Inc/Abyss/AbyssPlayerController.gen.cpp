// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Abyss/AbyssPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbyssPlayerController() {}
// Cross Module References
	ABYSS_API UClass* Z_Construct_UClass_AAbyssPlayerController_NoRegister();
	ABYSS_API UClass* Z_Construct_UClass_AAbyssPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_Abyss();
// End Cross Module References
	void AAbyssPlayerController::StaticRegisterNativesAAbyssPlayerController()
	{
	}
	UClass* Z_Construct_UClass_AAbyssPlayerController_NoRegister()
	{
		return AAbyssPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AAbyssPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAbyssPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_Abyss,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAbyssPlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "AbyssPlayerController.h" },
		{ "ModuleRelativePath", "AbyssPlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAbyssPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAbyssPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAbyssPlayerController_Statics::ClassParams = {
		&AAbyssPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAbyssPlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAbyssPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAbyssPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAbyssPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAbyssPlayerController, 3998521800);
	template<> ABYSS_API UClass* StaticClass<AAbyssPlayerController>()
	{
		return AAbyssPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAbyssPlayerController(Z_Construct_UClass_AAbyssPlayerController, &AAbyssPlayerController::StaticClass, TEXT("/Script/Abyss"), TEXT("AAbyssPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAbyssPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
